/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository;

import fit5042.tutex.repository.entities.Property;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO Exercise 1.3 Step 2 Complete this class.
 * 
 * This class implements the PropertyRepository class. You will need to add the keyword
 * "implements" PropertyRepository. 
 * 
 * @author Junyang, Qixin He
 */
public class SimplePropertyRepositoryImpl implements PropertyRepository{
	
	private ArrayList<Property> properties;
	

    public SimplePropertyRepositoryImpl() {
        this.properties = new ArrayList<Property>();
    }

    /**
     * Add the property being passed as parameter into the repository
     *
     * @param property - the property to add
     */
	@Override
	public void addProperty(Property property) throws Exception {
		this.properties.add(property);
		
	}

    /**
     * Search for a property by its property ID
     *
     * @param id - the propertyId of the property to search for
     * @return the property found
     */
	@Override
	public Property searchPropertyById(int id) throws Exception {
		for ( Property p : properties)
		{
			int searchid;
			searchid = p.getId();
			if (searchid == id)
					return p;
		}
		return null;
	}

    /**
     * Return all the properties in the repository
     *
     * @return all the properties in the repository
     */
	@Override
	public List<Property> getAllProperties() throws Exception {
		// TODO Auto-generated method stub
		return properties;
	}
    
}
