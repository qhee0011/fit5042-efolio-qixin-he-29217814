package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial instructions.
 * This is the main driver class. This class contains the main method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author Junyang
 */
public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;

    public RealEstateAgency(String name) throws Exception {
        this.name = name;
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
    }
    

	public PropertyRepository getPropertyRepository() {
		return propertyRepository;
	}

	// this method is for initializing the property objects
    // complete this method
    public void createProperty() throws Exception {
        Property property01 = new Property(1, "24 Boston Ave, Malvern East VIC 3145, Australia", 2, 150, 420000.00);
        Property property02 = new Property(2, "11 Bettina St, Clayton VIC 3168, Australia", 3, 352, 360000.00);
        Property property03 = new Property(3, "3 Wattle Ave, Glen Huntly VIC 3163, Australia", 5, 800, 650000.00);
        Property property04 = new Property(4, "3 Hamilton St, Bentleigh VIC 3240, Australia", 2, 170, 435000.00);
        Property property05 = new Property(5, "82 String Rd, Hampton East VIC 3188, Australia", 1, 60, 820000.00);

        propertyRepository.addProperty(property01);
        propertyRepository.addProperty(property02);
        propertyRepository.addProperty(property03);
        propertyRepository.addProperty(property04);
        propertyRepository.addProperty(property05);
        
        System.out.println("5 Properties added successfully");		
    }
    
    // this method is for displaying all the properties
    // complete this method
    public void displayProperties() throws Exception {
        String displayAllProperties = "";
        String priceint;
        for (Property p : propertyRepository.getAllProperties())
        {
        	displayAllProperties += p.getId();
        	displayAllProperties += " ";
        	displayAllProperties += p.getAddress();
        	displayAllProperties += " ";
        	displayAllProperties += p.getNumberOfBedrooms();
        	displayAllProperties += "BR(s) ";
        	displayAllProperties += p.getSize();
        	displayAllProperties += ".0sqm $";
        	priceint = String.valueOf(p.getPrice()) + "0";
        	int index = priceint.indexOf(".");
        	String priceintAddComma = priceint.substring(0,index-3) + "," + priceint.substring(index-3);
        	
        	
        	//int index = stringlist.size();
        	
        			
        	displayAllProperties += priceintAddComma;
        	displayAllProperties += "\n";
        	
        }
        System.out.println(displayAllProperties);
    }
    
    // this method is for searching the property by ID
    // complete this method
    public void searchPropertyById() throws Exception {
    	System.out.println("Enter the ID of the property you want to search:");
        Scanner scanner = new Scanner(System.in);
        int idInput = scanner.nextInt();
        Property propertyresult = propertyRepository.searchPropertyById(idInput);
        //ArrayList<Property> showTheProperty = new ArrayList<>();
        //showTheProperty.add(propertyresult);
        String displayResult = "";
        String priceint = "";
        displayResult += propertyresult.getId();
        displayResult += " ";
        displayResult += propertyresult.getAddress();
        displayResult += " ";
        displayResult += propertyresult.getNumberOfBedrooms();
        displayResult += "BR(s) ";
        displayResult += propertyresult.getSize();
        displayResult += ".0sqm $";
    	priceint = String.valueOf(propertyresult.getPrice());
    	int index = priceint.indexOf(".");
    	String priceintAddComma = priceint.substring(0,index-3) + "," + priceint.substring(index-3);

    	
    			
    	displayResult += priceintAddComma;
    	
    	System.out.print(displayResult);
    }
    
    public void run() throws Exception {
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    public static void main(String[] args) {
        try {
            new RealEstateAgency("FIT5042 Real Estate Agency").run();
        } catch (Exception ex) {
            System.out.println("Application fail to run: " + ex.getMessage());
        }
    }
}
