package fit5042.tutex.calculator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.ejb.Stateful;

import fit5042.tutex.repository.entities.Property;

@Stateful
public class ComparePropertySessionBean implements CompareProperty{
	private Set<Property> compareProperties;
	
 	public ComparePropertySessionBean()
 	{
 	compareProperties = new HashSet<>();
 	}
 	
	@Override
	public void addProperty(Property property) {
		// TODO Auto-generated method stub
		compareProperties.add(property);
	}

	@Override
	public void removeProperty(Property property) {
		// TODO Auto-generated method stub
		//compareProperties.remove(property);
		for (Property p : compareProperties) {
        	if (p.getPropertyId() == property.getPropertyId()) {
        		compareProperties.remove(p);
        		break;
        	}
        }
	}

	@Override
	public int getBestPerRoom() {
		// TODO Auto-generated method stub
		int comparePrice = 0;
		Integer bestID=0;
		if (!(compareProperties.isEmpty()))
		{
			double propertyPrice = 0;
			int propertyRoom = 0;
			
			ArrayList<String> perRoomPriceList = new ArrayList<>();
			ArrayList<String> perRoomIdList = new ArrayList<>();
			Iterator it = compareProperties.iterator();
			while (it.hasNext() == true)
			{
				Property property = (Property) it.next();
				propertyPrice = property.getPrice();
				propertyRoom = property.getNumberOfBedrooms();
				comparePrice = (int) (propertyPrice/propertyRoom);
				perRoomPriceList.add(String.valueOf(comparePrice));
				perRoomIdList.add(String.valueOf(property.getPropertyId()));
			}
			comparePrice = 0;
			
			for (String s : perRoomPriceList)
			{
				if (Integer.valueOf(s)< comparePrice || comparePrice == 0)
				{
					comparePrice = Integer.valueOf(s);
					
					}
			}
				
			int bestIdIndex = perRoomPriceList.indexOf(String.valueOf(comparePrice));
			bestID = Integer.valueOf(perRoomIdList.get(bestIdIndex));
		}
		
		return bestID;
	}

}
